namespace WebShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedratingtype : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductModels", "Rating", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductModels", "Rating");
        }
    }
}
