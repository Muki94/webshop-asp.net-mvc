﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.Models
{
    public class SubcategoryModels
    {
        public int Id { get; set; }
       
        public string Name { get; set; }

        public int CategoryModelsId { get; set; }
        public CategoryModels CategoryModels { get; set; }
    }
}