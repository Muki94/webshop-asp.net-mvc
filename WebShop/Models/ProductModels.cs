﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.Models
{
    public class ProductModels
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImagePath { get; set; }

        public double Price { get; set; }
        
        public int Rating { get; set; }

        public int SubcategoryModelsId { get; set; }
        public SubcategoryModels SubcategoryModels { get; set; }

        public List<ReviewModels> Reviews { get; set; }
        
        public DateTime Created_At { get; set; }
        
        public DateTime Updated_At { get; set; }
    }
}