﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebShop.Models
{
    public class ReviewModels
    {
        public int Id { get; set; }

        public string UserModelId { get; set; }
        public ApplicationUser UserModel { get; set; }

        public int ProductModelId { get; set; }
        public ProductModels ProductModel { get; set; }
        
        public DateTime Created_At { get; set; }

        public int Rating { get; set; }

        public string Description { get; set; }
    }
}