﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;
using Microsoft.AspNet.Identity;

namespace WebShop.Controllers
{
    public class ReviewController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // POST: Review/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                if (User.Identity.IsAuthenticated)
                {
                    // TODO: Add insert logic here
                    ReviewModels review = new ReviewModels();

                    review.ProductModelId = Int32.Parse(collection["productId"]);
                    if (review.ProductModelId == 0)
                    {
                        return Json(new { success = false, responseText = "You are not reviewing any product!" });
                    }

                    review.Description = collection["comment"];
                    if (review.Description == String.Empty)
                    {
                        return Json(new { success = false, responseText = "You have to add a description in order to finish review!" });
                    }

                    int rating;
                    Int32.TryParse(collection["selectedStar"],out rating);
                    review.Rating = rating;
                    if (review.Rating == 0)
                    {
                        return Json(new { success = false, responseText = "You have to rate the product in order to finish review!" });
                    }

                    review.Created_At = DateTime.Now;
                    review.UserModelId = User.Identity.GetUserId();

                    db.ReviewModels.Add(review);
                    db.SaveChanges();
                    return Json(new {success = true});
                }
                else
                {
                    return Json(new {success = false,responseText = "You are not loged in so you cant review the product!" });
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Review/Edit/5
        [HttpGet]
        public ActionResult Admin_Review_Edit(int reviewId)
        {
            return PartialView(db.ReviewModels.Find(reviewId));
        }

        // POST: Review/Edit/5
        [HttpPost]
        public ActionResult Admin_Review_Edit(ReviewModels reviewModel)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    ReviewModels review = db.ReviewModels.Find(reviewModel.Id);

                    if (review == null)
                        return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                    List<string> allErrors = new List<string>();

                    if (reviewModel.Description == null || reviewModel.Description == String.Empty)
                        allErrors.Add("Description is required!");
                    if (allErrors.Count > 0)
                        return Json(new { statusCode = 400, success = false, responseMessage = allErrors });

                    review.Description = reviewModel.Description;
                    review.Rating = reviewModel.Rating;
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Review Edited!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unauthorized!" });
            }
            catch
            {
                return View();
            }
        }

        // POST: Review/Delete/5
        [HttpPost]
        public ActionResult Admin_Review_Delete(int reviewId)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    ReviewModels review = db.ReviewModels.Find(reviewId);

                    if (review == null)
                        return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                    db.ReviewModels.Remove(review);
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Review Deleted!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unaothorized!" });
            }
            catch
            {
                return View();
            }
        }
    }
}
