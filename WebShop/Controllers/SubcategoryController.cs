﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class SubcategoryController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        // GET: Subcategory
        [HttpGet]
        public ActionResult Admin_Subcategory_Index()
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                    return PartialView(db.SubcategoryModel.Include("CategoryModels").ToList());

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Subcategory/Create
        [HttpGet]
        public ActionResult Admin_Subcategory_Create()
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin")) { 
                    ViewBag.categories = db.CategoryModel.ToList();
                    return PartialView();
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Subcategory/Create
        [HttpPost]
        public ActionResult Admin_Subcategory_Create(SubcategoryModels subcategoryModel)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    List<string> allErrors = new List<string>();

                    if (subcategoryModel.Name == null || subcategoryModel.Name == String.Empty)
                        allErrors.Add("Name is required!");
                    if (subcategoryModel.CategoryModelsId == null)
                        allErrors.Add("Category is required!");

                    if (allErrors.Count > 0)
                        return Json(new { statusCode = 400, success = false, responseMessage = allErrors });

                    db.SubcategoryModel.Add(subcategoryModel);
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Subcategory Added!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Subcategory/Edit/5
        [HttpGet]
        public ActionResult Admin_Subcategory_Edit(int subcategoryId)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                SubcategoryModels subcategory = db.SubcategoryModel.Find(subcategoryId);

                if (subcategory == null)
                    return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                return PartialView(subcategory);
            }

            return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
        }

        // POST: Subcategory/Edit/5
        [HttpPost]
        public ActionResult Admin_Subcategory_Edit(SubcategoryModels subcategoryModel)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    SubcategoryModels subcategory = db.SubcategoryModel.Find(subcategoryModel.Id);

                    if (subcategory == null)
                        return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                    List<string> allErrors = new List<string>();

                    if (subcategoryModel.Name == null || subcategoryModel.Name == String.Empty)
                        allErrors.Add("Name is required!");
                    if (subcategoryModel.CategoryModelsId == null)
                        allErrors.Add("Category is required!");

                    if (allErrors.Count > 0)
                        return Json(new { statusCode = 400, success = false, responseMessage = allErrors });

                    subcategory.Name = subcategory.Name;
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Category Edited!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Subcategory/Delete/5
        [HttpPost]
        public ActionResult Admin_Subcategory_Delete(int subcategoryId)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    SubcategoryModels subcategory = db.SubcategoryModel.Find(subcategoryId);
                    if (subcategory == null)
                        return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                    db.SubcategoryModel.Remove(subcategory);
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Category Deleted!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch
            {
                return View();
            }
        }
    }
}
