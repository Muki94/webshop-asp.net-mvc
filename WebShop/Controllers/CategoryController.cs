﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class CategoryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Category
        [HttpGet]
        public ActionResult Admin_Category_Index()
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                    return PartialView(db.CategoryModel.ToList());

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Category/Create
        [HttpGet]
        public ActionResult Admin_Category_Create()
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                    return PartialView();

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Admin_Category_Create(CategoryModels categoryModel)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin")) {
                    List<string> allErrors = new List<string>();

                    if (categoryModel.Name == null || categoryModel.Name == String.Empty)
                        allErrors.Add("Name is required!");

                    if (allErrors.Count > 0)
                        return Json(new { statusCode = 400, success = false, responseMessage = allErrors });

                    db.CategoryModel.Add(categoryModel);
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Category Added!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Category/Edit/5
        [HttpGet]
        public ActionResult Admin_Category_Edit(int categoryId)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                CategoryModels category = db.CategoryModel.Find(categoryId);

                if (category == null)
                    return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                return PartialView(category);
            }

            return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Admin_Category_Edit(CategoryModels categoryModel)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    CategoryModels category = db.CategoryModel.Find(categoryModel.Id);

                    if (category == null)
                        return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });
                    
                    List<string> allErrors = new List<string>();

                    if (categoryModel.Name == null || categoryModel.Name == String.Empty)
                        allErrors.Add("Name is required!");

                    if (allErrors.Count > 0)
                        return Json(new { statusCode = 400, success = false, responseMessage = allErrors });

                    category.Name = categoryModel.Name;
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Category Edited!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Category/Delete/5
        [HttpPost]
        public ActionResult Admin_Category_Delete(int categoryId)
        {
            try
            {
                if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                {
                    CategoryModels category = db.CategoryModel.Find(categoryId);
                    if (category == null)
                        return Json(new { statusCode = 404, success = false, responseMessage = "Not found!" });

                    db.CategoryModel.Remove(category);
                    db.SaveChanges();

                    return Json(new { statusCode = 200, success = true, responseMessage = "Category Deleted!" });
                }

                return Json(new { statusCode = 401, success = false, responseMessage = "Unathorized!" });
            }
            catch
            {
                return View();
            }
        }
    }
}
