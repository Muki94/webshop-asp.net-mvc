﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Controllers
{
    public class ProductController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// User Section
        /// </summary>
        #region User Section
        // GET: ProductModels
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        // GET: ProductModels/Index
        [HttpGet]
        public PartialViewResult Index_Partial(int selectedPage = 0, int elementsPerPage = 5, int selectedCategory = 0, int selectedSubcategory = 0, int price = Int32.MaxValue)
        {
            List<ProductModels> products = db.ProductModel.Where(x => x.SubcategoryModelsId == selectedSubcategory).ToList();

            for (int i = 0; i < products.Count; i++)
                if (products[i].Description != null && products[i].Description.Length > 40)
                {
                    products[i].Description = products[i].Description.Substring(0, 40) + "...";
                }

            //Get all categories and selected category
            ViewBag.categories = db.CategoryModel.ToList();
            ViewBag.selectedCategory = selectedCategory;

            //Get all subcategories and selected subcategory
            ViewBag.subcategories = db.SubcategoryModel.Where(x => x.CategoryModelsId == selectedCategory).ToList();
            ViewBag.selectedSubcategory = selectedSubcategory;

            ViewBag.numberOfPages = Math.Ceiling((double)products.Count / elementsPerPage);
            ViewBag.selectedPage = selectedPage;
            ViewBag.elementsPerPage = elementsPerPage;
            ViewBag.price = price;

            //Apply all search criteria
            products = products.Where(x => x.Price <= price)
                               .OrderBy(x => x.Id)
                               .Skip(selectedPage * elementsPerPage)
                               .Take(elementsPerPage)
                               .ToList();

            return PartialView(products);
        }

        // GET: ProductModels/Details/5
        [HttpGet]
        public ActionResult Details(int id)
        {
            ProductModels productModels = db.ProductModel.Where(x => x.Id == id).FirstOrDefault();
            List<ReviewModels> reviews = db.ReviewModels.Include(x => x.UserModel).Where(x => x.ProductModelId == id).OrderByDescending(x => x.Created_At).ToList();

            double rating = 0;

            if (productModels == null)
            {
                return HttpNotFound();
            }

            foreach (var item in reviews)
                rating += item.Rating;

            rating = rating / reviews.Count + 0.5;

            productModels.Rating = (int)rating;
            productModels.Reviews = reviews;

            return View(productModels);
        }
        #endregion

        /// <summary>
        /// Admin Section
        /// </summary>
        #region Admin Section
        // GET: ProductModels/Index
        [HttpGet]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Index()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                return PartialView("Admin_Index_Partial", db.ProductModel.Include(x => x.SubcategoryModels).ToList());

            return Redirect("/Admin/Index");
        }

        [HttpGet]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Details(int productId)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                return PartialView("Admin_Details_Partial", db.ProductModel.Include(x => x.SubcategoryModels)
                                                                           .Include(x => x.Reviews)
                                                                           .Include("Reviews.UserModel")
                                                                           .Where(x => x.Id == productId)
                                                                           .FirstOrDefault());

            return Redirect("/Admin/Index");
        }

        // GET: ProductModels/Create
        [HttpGet]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Create()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                List<SelectListItem> subcategories = db.SubcategoryModel.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }).ToList();

                ViewBag.subcategories = subcategories;

                return PartialView("Admin_Create_Partial");
            }

            return Redirect("/Admin/Index");
        }

        // POST: ProductModels/Create
        [HttpPost]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Create(ProductModels productModels)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                List<string> allErrors = new List<string>();

                if (productModels.Name == null || productModels.Name == String.Empty)
                    allErrors.Add("Name is required!");
                if (productModels.Price == 0)
                    allErrors.Add("Price is required!");

                if (allErrors.Count > 0)
                    return Json(new {statusCode = 400, success = false, responseMessage = allErrors });

                productModels.Created_At = DateTime.Now;
                productModels.Updated_At = DateTime.Now;
                productModels.Rating = 0;
                db.ProductModel.Add(productModels);
                db.SaveChanges();
                return Json(new {statusCode = 200, success = true, responseMessage = "Product Added!" });
            }

            return Json(new { statusCode = 401 });
        }

        // GET: ProductModels/Edit
        [HttpGet]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Edit(int productId)
        {
            var product = db.ProductModel.Find(productId);

            if (product == null)
                return HttpNotFound();

            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                List<SelectListItem> subcategories = db.SubcategoryModel.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString(),
                    Selected = product.SubcategoryModelsId == x.Id?true:false
                }).ToList();

                ViewBag.subcategories = subcategories;

                return PartialView("Admin_Edit_Partial",product);
            }

            return Redirect("/Admin/Index");
        }

        // PUT: ProductModels/Edit
        [HttpPost]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Edit(ProductModels productModel)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                List<string> allErrors = new List<string>();

                if (productModel.Name == null || productModel.Name == String.Empty)
                    allErrors.Add("Name is required!");
                if (productModel.Price == 0)
                    allErrors.Add("Price is required!");

                if (allErrors.Count > 0)
                    return Json(new { statusCode = 400, success = false, responseMessage = allErrors });

                var product = db.ProductModel.Find(productModel.Id);
                product.ImagePath = productModel.ImagePath != null ? productModel.ImagePath : product.ImagePath;
                product.Name = productModel.Name != null && productModel.Name != String.Empty ? productModel.Name : product.Name;
                product.Price = productModel.Price != 0 ? productModel.Price : product.Price;
                product.SubcategoryModelsId = productModel.SubcategoryModelsId != 0 ? productModel.SubcategoryModelsId : product.SubcategoryModelsId;
                product.Updated_At = DateTime.Now;

                db.SaveChanges();
                return Json(new { statusCode = 200, success = true, responseMessage = "Product Edited!" });
            }

            return Json(new { statusCode = 401 });
        }

        // DELETE: ProductModels/Delete
        [HttpPost]
        [ApplicationAuthorize]
        public ActionResult Admin_Product_Delete(int productId)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
            {
                db.ProductModel.Remove(db.ProductModel.Find(productId));
                db.SaveChanges();
                return Json(new { statusCode = 200, success = true, responseMessage = "Product Deleted!" });
            }

            return Json(new { statusCode = 401 });
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
