﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Web.Security;
using System.Net;

namespace WebShop.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AdminController()
        {
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        //UserManager
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        
        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                return RedirectToAction("AdminPanel");

            return View();
        }

        // POST: Admin/Login
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            var user = UserManager.FindByEmail(username);

            if (user == null)
                return Json(new { success = false, responseMessage = "Login credentials are not correct!" });

            if (UserManager.IsInRole(user.Id, "Admin"))
            {
                var result = SignInManager.PasswordSignIn(user.UserName, password, false, false);

                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToAction("AdminPanel");
                    default:
                        return Json(new { success = false, responseMessage = "Action not permitted!" });
                }
            }

            return RedirectToAction("Index");
        }

        // Get: Admin/Login
        [HttpGet]
        public ActionResult LogOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index");
        }

        // GET: Admin/AdminPanel
        [HttpGet]
        public ActionResult AdminPanel()
        {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                return View();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MainPanel() {
            if (User.Identity.IsAuthenticated && User.IsInRole("Admin"))
                return PartialView();

            return RedirectToAction("Index");
        }

        public ActionResult Login() {
            return RedirectToAction("Index");
        }
    }
}
